import boto3
import hashlib
import json
import urllib2

SERVICE = "CLOUDFRONT"

IP_RANGES_URL = "https://ip-ranges.amazonaws.com/ip-ranges.json"

INGRESS_PORTS = { 'Http' : 80, 'Https': 443 }

def lambda_handler():
    # Load the ip ranges from the url
    ip_ranges = json.loads(get_ip_groups_json(IP_RANGES_URL))

    # extract the service ranges
    cf_ranges = get_ranges_for_service(ip_ranges, SERVICE)

    # return list of ingress rules
    return get_security_group_ingress(cf_ranges)


def get_ip_groups_json(url):
    print("Updating from " + url)
    response = urllib2.urlopen(url)
    ip_json = response.read()

    return ip_json

def get_ranges_for_service(ranges, service):
    service_ranges = list()
    for prefix in ranges['prefixes']:
        if prefix['service'] == service:
            print('Found ' + service + ' range: ' + prefix['ip_prefix'])
            service_ranges.append(prefix['ip_prefix'])

    return service_ranges

def get_security_group_ingress(cidr_list):
	sg_ingress_rules = list()
	for cidr in cidr_list:
		sg_ingress_rules.append({ "IpProtocol": "HTTP","FromPort": "80","ToPort": "80","CidrIp": cidr })
		sg_ingress_rules.append({ "IpProtocol": "HTTPS","FromPort": "443","ToPort": "443","CidrIp": cidr })
	return sg_ingress_rules
          