#!/usr/bin/python
 
'''
Program to fetch whois information of a domain name
chris Onare
conare@deloitte.com.au
'''
import socket, sys, csv
 
#Perform a generic whois query to a server and get the reply
def perform_whois(server , query) :
	#socket connection
	s = socket.socket(socket.AF_INET , socket.SOCK_STREAM)
	s.connect((server , 43))
	 
	#send data
	s.send(query + '\r\n')
	 
	#receive reply
	msg = ''
	while len(msg) < 10000:
		chunk = s.recv(100)
		if(chunk == ''):
			break
		msg = msg + chunk
	 
	return msg
	s.close()
#End
 
#Function to perform the whois on a domain name
def get_whois_data(domain):
	 
	#remove http, https and www
	domain = domain.replace('http://','')
	domain = domain.replace('https://','')
	domain = domain.replace('www.','')
	 
	#get the extension , .com , .org , .edu
	ext = domain[-3:]
	 
	#If top level domain .com .org .net
	if(ext == 'com' or ext == 'org' or ext == 'net'):
		whois = 'whois.internic.net'
		msg = perform_whois(whois , domain)
		 
		#Now scan the reply for the whois server
		lines = msg.splitlines()
		for line in lines:
			if ':' in line:
				words = line.split(':')
				if  'Whois' in words[0] and 'whois.' in words[1]:
					whois = words[1].strip()
					break;
	 
	#Or Country level - contact whois.iana.org to find the whois server of a particular TLD
	else:
		#Break again like , co.uk to uk
		ext = domain.split('.')[-1]
		 
		#This will tell the whois server for the particular country
		whois = 'whois.iana.org'
		msg = perform_whois(whois , ext)
		 
		#Now search the reply for a whois server
		lines = msg.splitlines()
		for line in lines:
			if ':' in line:
				words = line.split(':')
				if 'whois.' in words[1] and 'Whois Server (port 43)' in words[0]:
					whois = words[1].strip()
					break;
	 
	#Now contact the final whois server
	msg = perform_whois(whois , domain)

	return filter_response(msg, domain)

# end

def filter_response(response, domain):
	# create list to store filtered response
	domain_results = []
	# return the reply
	response = response.lower()
	response = response.split('\n')
	name_servers = 'nservers:'
	registrar = 'registrar:'
	organization = 'organization:'
	registry_expiry = 'registry_expiry:'
	for line in response:
		line = line.rstrip().lstrip()
		if 'organization:' in line:
			organization += organization
		elif 'name server:' in line:
			name_servers += line.split(":")[1] + ';'
		elif 'nserver:' in line:
			name_servers += line.split(":")[1] + ';'
		elif 'registrar:' in line:
			registrar += line.split(":")[1]
		elif 'registry expiry date:' in line:
			registry_expiry += line.split(":")[1]
	# return string response in the following order : domain, organization, nameservers, registrar, registry_expiry
	result = 'domain:' + domain + '|' + organization + '|' + name_servers + '|' + registrar + '|' + registry_expiry
	print result
	return result


# function to open a csv file containing dns names and return contents as a list
def read_csv_to_list(filepath):
	# create a list placeholder 
	domain_names_list = []
	# open file for reading
	with open(filepath, 'rb') as f:
		# read the csv contents
		reader = csv.reader(f)
		# Loop through the content of each row in reader (domain) and add it to domain list
		for row in reader:

			domain_names_list.extend(row)
	# return domain list to caller method
	return domain_names_list
# function to write list to csv file
def write_list_to_csv(write_file_path, reader_file_path):
	# open file for writing
	csv = open(write_file_path, 'w')
	# create generic title row
	columnTitleRow = "Domain, Organization, Name Servers, Registrar, Registry Expiry\n"
	# write title to file first
	csv.write(columnTitleRow)
	
	domains = read_csv_to_list(reader_file_path)
	# do whois search on each domain and return search results to domain
	for domain in domains:
		# loop over and write each row to csv file
		result = get_whois_data(domain).split('|')
		domain = ''
		organization = ''
		nservers = ''
		registrar = ''
		registry_expiry = ''
		for item in result:
			if 'domain:' in item:
				domain = get_item(item)
			elif 'organization:' in item:
				organization = get_item(item)
			elif 'nservers:' in item:
				nservers = get_item(item)
			elif 'registrar:' in item:
				registrar = get_item(item)
			elif 'registry_expiry:' in item:
				registry_expiry = get_item(item)

			# create row for csv
			#registrar = item.registrar

			#ns_servers = item.ns_servers
			# create a row containing all the dns details
			#row = registrar + ',' + ns_servers
			# write row to csv file
		dns_response = domain + ',' + organization + ',' + nservers + ',' + registrar + ',' + registry_expiry + '\n'
		csv.write(dns_response)
# function to get domain names

def get_item(item):
	result = item.split(":")[1]
	if  result == '':
		result = 'null'
	
	return result

# get the file path from command line argument
file_path_to_read_csv = sys.argv[1]
file_path_to_write_csv = sys.argv[2]

print write_list_to_csv(file_path_to_write_csv, file_path_to_read_csv)