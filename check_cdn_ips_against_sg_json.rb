require 'net/http'
require 'uri'
require 'fileutils'
require 'json'

uri = URI.parse("http://d7uri8nf7uskq.cloudfront.net/tools/list-cloudfront-ips")
filename = 'file.json'
response = Net::HTTP.get_response(uri)
cdn_ips = {}
new_cdn_list = []
cdn_ips = eval(response.body)
cdn_ips.each do |name, body|
	body.each do |cidr|
		new_cdn_list << cidr
	end
	
end

current_dir = File.expand_path File.dirname(__FILE__)

if File.exist?("#{current_dir}/#{filename}")
	json_blob = JSON.load(File.read("#{current_dir}/#{filename}"))
end

lb_cidr_ips = []
json_blob['Resources']['SecurityGroupName']['Properties']['SecurityGroupIngress'].each do |block|
	lb_cidr_ips << block['CidrIp']
end

# Ips not in ENBNE
ips_not_updated = new_cdn_list - lb_cidr_ips

puts ips_not_updated